#1
my_list = ['hello', 'how', 'are', 'you', 'doing', 'today']

def func1(my_list):
    new_list = my_list.copy()
    for i in range(len(new_list)):
        if i % 2 == 0:
            new_list[i] = new_list[i][::-1]
    return new_list

print(func1(my_list))
#######################
#2
my_list = ['hello', 'how', 'are', 'you', 'doing', 'today']

def func2(my_list):
    new_list = []
    for i in my_list:
        if i[0].lower() == 'a':
            new_list.append(i)
    return new_list

print(func2(my_list))
#######################
#3
my_list = ['hello', 'how', 'are', 'you', 'doing', 'today']

def func3(my_list):
    new_list = []
    for i in my_list:
        if i.find('a') != -1 or i.find('A') != -1:
            new_list.append(i)
    return new_list

print(func3(my_list))
#######################
#4
my_list = ['hello', 'how', 'are', 'you', 'doing', 'today', 123, 456]

def func4(my_list):
    new_list = []
    for i in my_list:
        if type(i) == str:
            new_list.append(i)
    return new_list

print(func4(my_list))
#######################
#5
my_str = 'wewhwajkehdskeujlsdkpwoqi ejf sdkfhesesfjdkjlskjdlf'

def func5(my_str):
    chars = []
    for i in my_str:
        if list(my_str).count(i) == 1:
            chars.append(i)
    return list(set(chars))

print(func5(my_str))
#######################
#6
first_str = 'kejlkqwj lkejqkwjel kjw; lajelij sk;kfodfk;s'
second_str = 'ejqwh jdjkalksjdlkja owjoidkal;skdl;k'

def func6(first_str, second_str):
    return list(set(first_str) & set(second_str))

print(func6(first_str, second_str))
#######################
#7
first_str = 'kejlkqwj lkejqkwjel kjw; lajelij sk;kfodfk;s'
second_str = 'ejqwh jdjkalksjdlkja owjoidkal;skdl;k'

def func7(first_str, second_str):
    first_chars = []
    second_chars = []
    for i in first_str:
        if list(first_str).count(i) == 1:
            first_chars.append(i)
    for i in second_str:
        if list(second_str).count(i) == 1:
            second_chars.append(i)
    return list(set(first_chars) & set(second_chars))
print(func7(first_str, second_str))
#######################
#8
import random
names = ["aloha", "schwartz", "gray"]
domains = ["net", "com", "ua"]

def func8(names, domains):
    name = random.choice(names)
    domain = random.choice(domains)
    num = random.randint(100, 999)
    letters = 'abcdefghijklmnopqrstuvwxyz'
    string = ''
    for i in range(random.randint(5, 7)):
        string += random.choice(letters)
    return f"{name}.{num}@{string}.{domain}"
print(func8(names, domains))
#######################