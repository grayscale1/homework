def course(currency):
    try:
        rate = None
        balance = None   
        with open(f'{currency}.txt', 'r') as f:
            file = f.readlines()
        for row in file:
            r = row.split(':')
            if r[0] == 'RATE':
                rate = float(r[1])
            elif r[0] == 'BALANCE':
                balance = float(r[1])
        return f'RATE {rate}, AVAILABLE {balance}'
    except:
        return f'INVALID CURRENCY {currency}'

def exchange(currency1, amount):
    try:
        rate1 = None
        balance1 = None
        rate2 = None
        balance2 = None
        currency2 = None
        if currency1 == 'UAH':
            currency2 = 'USD'
        else:
            currency2 = 'UAH'

        with open(f'{currency1}.txt', 'r') as f:
            file = f.readlines()
        for row in file:
            r = row.split(':')
            if r[0] == 'RATE':
                rate1 = float(r[1])
            elif r[0] == 'BALANCE':
                balance1 = float(r[1])

        with open(f'{currency2}.txt', 'r') as f:
            file = f.readlines()
        for row in file:
            r = row.split(':')
            if r[0] == 'RATE':
                rate2 = float(r[1])
            elif r[0] == 'BALANCE':
                balance2 = float(r[1])
      
        if balance2 >= amount * rate2:
            with open(f'{currency1}.txt', 'w') as f:
                f.write(f'RATE:{rate1}\nBALANCE:{balance1-amount}')
            with open(f'{currency2}.txt', 'w') as f:
                f.write(f'RATE:{rate2}\nBALANCE:{balance2+amount*rate2}')
            return f'{currency2} {amount*rate2}, RATE {rate2}'
        else:
            return f'UNAVAILABLE, REQUIRED BALANCE {currency2} {amount*rate2}, AVAILABLE {balance2}'
    except:
        return f'INVALID CURRENCY {currency1}'

if __name__ == '__main__':
    while True:
        case = input('>>> ')
        if case.split()[0] == 'COURSE':
            print(course(case.split()[1]))
        elif case.split()[0] == 'EXCHANGE':
            print(exchange(case.split()[1], float(case.split()[2])))
        elif case == 'STOP':
            print('SERVICE STOPPED')
            break
        else:
            print(f'INVALID COMMAND {case}')