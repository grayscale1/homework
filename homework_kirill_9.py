#1
def func9():
    with open('file.txt', 'r', encoding='utf-8') as file:
        arr = [i.replace('\n', '').split(' ', 2) for i in file.readlines()]
    new_arr = []
    average_less_5 = []
    for i in arr:
        marks = i[2].split(' ')
        average = 0
        for j in marks:
            average += int(j)
        average = round(average / len(marks), 2)
        if average < 5:
            average_less_5.append(f"{i[0]} {i[1]} {average}")
        new_arr.append(f"{i[0]} {i[1][0]}. {average}")
    print('\n'.join(average_less_5))
    with open('new_file.txt', 'w', encoding='utf-8') as file:
        file.write('\n'.join(new_arr))
###############
#2
def func10():
    text_input = []
    while True:
        text = input('>>> ')
        if text == '':
            break
        else:
            text_input.append(text)
    with open('file.txt', 'w') as file:
        file.write('\n'.join(text_input))
################