#Homework 5
#1. Дано целое число (int). Определить сколько нулей в этом числе.
my_num = 12300213423000000
str_num = str(my_number)
count_zeros = str_num.count('0')
print(count_zeros)
###############

#2. Дано целое число (int). Определить сколько нулей в конце этого числа. Например для числа 1002000 - три нуля
my_num = 12300213423000000
zeros = 0
str_num = str(my_num)
for zero in string_num[::-1]:
    if zero == '0':
        zeros += 1
    else:
        break
print(zeros)
################

#3. Даны списки my_list_1 и my_list_2.
#Создать список my_result в который вначале поместить
#элементы на четных местах из my_list_1, а потом все элементы на нечетных местах из my_list_2.

my_list_1 = ['One', 'Two', 'Three', 'Four', 'Five']
my_list_2 = ['Six', 'Seven', 'Eight', 'Nine']

my_result = my_list_1[::2] + my_list_2[1::2]
print(my_result)
################
#4. Дан список my_list. СОЗДАТЬ НОВЫЙ список new_list у которого первый элемент из my_list
#стоит на последнем месте. Если my_list [1,2,3,4], то new_list [2,3,4,1]

my_list = [1, 2, 3, 4]
new_list = my_list[1:] + [my_list[0]]
print(new_list)
################

#5.Дан список my_list. В ЭТОМ списке первый элемент переставить на последнее место.
#[1,2,3,4] -> [2,3,4,1]. Пересоздавать список нельзя! (используйте метод pop)

my_list = [1, 2, 3, 4]
value = my_list.pop(0)
my_list.append(value)
print(my_list)
################

#6. Дана строка в которой есть числа (разделяются пробелами).
#Например "43 больше чем 34 но меньше чем 56". Найти сумму ВСЕХ ЧИСЕЛ (А НЕ ЦИФР) в этой строке.
#Для данного примера ответ - 133. (используйте split и проверку isdigit)

my_string = "56 больше чем 22 но меньше чем 87"
my_str_list = my_string.split()
counter = 0

for item in my_str_list:
    if item.isdigit():
        counter += int(item)
print(counter)
#################

#7. Дана строка my_str в которой символы МОГУТ повторяться и два символа l_limit, r_limit,
#которые точно находятся в этой строке. Причем l_limit левее чем r_limit.
#В переменную sub_str поместить НАИБОЛЬШУЮ часть строки между этими символами.
#my_str = "My long string", l_limit = "o", r_limit = "g" -> sub_str = "ng strin".

my_str = "My long string"
l_limit = "o"
r_limit = "g"
l_index = my_str.find(l_limit) +1
r_index = my_str.rfind(r_limit)
sub_str = my_str[l_index: r_index]
print(sub_str)

################
#8. Дана строка my_str. Разделите эту строку на пары из двух символов и поместите эти пары в список.
#Если строка содержит нечетное количество символов, пропущенный второй символ последней пары должен
#быть заменен подчеркиванием ('_'). Примеры: 'abcd' -> ['ab', 'cd'], 'abcde' -> ['ab', 'cd', e_']
#(используйте срезы длинны 2)

example = 'ewqjhejqwhekqhw'

result_list = []
EXAMPLE_LENGHT = len(example)
for index in range(0, EXAMPLE_LENGHT, 2):
    if index < EXAMPLE_LENGHT - 1:
        result_list.append(example[index] + example[index + 1])
    else:
        result_list.append(example[index] + '_')
print(result_list)

##################
#9. Дан список чисел. Определите, сколько в этом списке элементов,
#которые больше суммы двух своих соседей (слева и справа), и НАПЕЧАТАЙТЕ КОЛИЧЕСТВО таких элементов.
#Крайние элементы списка никогда не учитываются, поскольку у них недостаточно соседей.
#Для списка [2,4,1,5,3,9,0,7] ответом будет 3 потому что 4 > 2+1, 5 > 1+3, 9>3+0.

my_list = [2,4,1,5,3,9,0,7,3,2,3,1,2,7,9,3,5,6,7]
counter = 0
for index in range(1, len(my_list) - 1):
    if my_list[index] > my_list[index -1]  + my_list[index +1]:
        counter += 1
print(counter)
######################