#1.a
my_dict = [{"name": "John", "age": 15}, {"name": "Ivan", "age": 33},
           {"name": "Alicia", "age": 15}, {"name": "Martin", "age": 45}]

youngest = sorted(my_dict, key=lambda item: item["age"], reverse=False)
for index in youngest:
    if index.get("age") == youngest[0].get("age"):
        print(index.get("name"))
######################
#1.b
my_dict = [{"name": "John", "age": 15}, {"name": "Ivan", "age": 33},
           {"name": "Alicia", "age": 15}, {"name": "Martin", "age": 45}]

longest_name = sorted(my_dict, key=lambda item: len(item["name"]), reverse=True)
for index in longest_name:
    if len(index.get("name")) == len(longest_name[0].get("name")):
        print(index.get("name"))
######################
#1.c
my_dict = [{"name": "John", "age": 15}, {"name": "Ivan", "age": 33},
           {"name": "Alicia", "age": 15}, {"name": "Martin", "age": 45}]

avg_age = 0
for index in my_dict:
    avg_age += index.get("age")
if len(my_dict) > 0:
    result = avg_age/len(my_dict)
else:
    result = "I don't know"
print(result)
######################
#2.a
my_dict_1 = {"name": "John", "age": 15, "gender": "male"}
my_dict_2 = {"name": "Jack", "age": 22, "weight": 68}

the_list = list(set(list(my_dict_1)).intersection(list(my_dict_2)))
print(the_list)
#####################
#2.b
my_dict_1 = {"name": "John", "age": 15, "gender": "male"}
my_dict_2 = {"name": "Jack", "age": 22, "weight": 68}

the_list = list(set(list(my_dict_1)).difference(list(my_dict_2)))
print(the_list)
#####################
#2.c
my_dict_1 = {"name": "John", "age": 15, "gender": "male"}
my_dict_2 = {"name": "Jack", "age": 22, "weight": 68}

the_list = list(set(list(my_dict_1)).difference(list(my_dict_2)))
my_dict_3 = {}
for item in the_list:
    my_dict_3[item] = my_dict_1.get(item)
print(my_dict_3)
######################
#2.d
my_dict_1 = {"name": "John", "age": 15, "gender": "male"}
my_dict_2 = {"name": "Jack", "age": 22, "weight": 68}

the_list = list(set(list(my_dict_1)).intersection(list(my_dict_2)))

my_dict_4 = my_dict_1
my_dict_4.update(my_dict_2)
for item in the_list:
    my_dict_4[item] = my_dict_1[item], my_dict_2[item]
print(my_dict_4)
